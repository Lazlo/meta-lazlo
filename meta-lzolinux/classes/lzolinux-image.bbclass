IMAGE_FEATURES += "package-management splash ssh-server-openssh x11-base hwcodecs"
IMAGE_FEATURES += "dev-pkgs"

IMAGE_INSTALL = "\
    packagegroup-core-boot \
    packagegroup-core-full-cmdline \
    ${CORE_IMAGE_EXTRA_INSTALL} \
    linux-firmware \
    \
    dosfstools \
    \
    htop \
    tree \
    vim \
    git \
    wget \
    rsync \
    dnsmasq \
    openvpn \
    hostapd \
    ethtool \
    nmap \
    tcpdump \
    socat \
    tmux \
    screen \
    cpufrequtils \
    i2c-tools \
    can-utils \
    btrfs-tools \
    \
    tigervnc \
    "

inherit core-image distro_features_check

REQUIRED_DISTRO_FREATURES = "x11"

QB_MEM = '${@bb.utils.contains("DISTRO_FEATURES", "opengl", "-m 512", "-m 256", d)}'
