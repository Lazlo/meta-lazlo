@Library('pipeline-library') _

pipeline {
    agent {
        node {
            // Do not set a label. Thereby making sure we are not
            // executed on master but on a build slave.
            label ''
            customWorkspace "${JOB_NAME.replace('%', '')}/build_${BUILD_NUMBER}"
        }
    }
    environment {
        /* Locks */
        APT_LOCK    = "apt-lock-${env.NODE_NAME}"
    }
    stages {
        stage('Setup') {
            steps {
                timeout(time: 5, unit: 'MINUTES') {
                    retry(5) {
                        lock("${env.APT_LOCK}") {
                            echo 'Setting up ...'
                            sh 'sudo ./scripts/setup.sh'
                        }
                    }
                }
            }
        }
        stage('Fetch') {
            steps {
                echo 'Fetching ...'
                sh './scripts/fetch.sh'
            }
        }
        stage('Build') {
            environment {
                LANG = 'en_US.UTF-8'
                TMP_BASE_DIR    = '/var/build/yocto'
                DL_DIR          = '/var/build/yocto/downloads'
                SSTATE_DIR      = '/var/build/yocto/sstate-cache'
            }
            steps {
                echo 'Building ...'
                sh 'mkdir -p $TMP_BASE_DIR'
                // For some reason we need to do set +x to keep whatever
                // Jenkins calls for sh() from printing the contents
                // of the scripts sourced.
                //
                // Note that $IMAGE is provided when sourcing the lzo-init-build-env script
                sh '''set +x
                . ./lzo-init-build-env && bitbake $IMAGE

                echo "Compressing .wic file ..."
                pbzip2 tmp/deploy/images/$MACHINE/$IMAGE-$MACHINE.wic

                echo "Creating archive containing compressed .wic and .wic.bmap files ..."
                tar cvjf ../$IMAGE-$MACHINE.tar.bz2 \
                    -C tmp/deploy/images/$MACHINE \
                    $IMAGE-$MACHINE.wic.bz2 \
                    $IMAGE-$MACHINE.wic.bmap
                '''
                archiveArtifacts artifacts: "*.tar.bz2"
            }
        }
    }
    post {
        always {
            /* Save space on our build server. */
            sh './scripts/clean.sh'
            commonStepNotification()
        }
    }
}
