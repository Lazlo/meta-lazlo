#!/bin/sh

set -e
set -u

: "${BRANCH:=dunfell}"

git_clone() {
    url="$1"
    if [ $# -gt 1 ]; then
        branch="$2"
    else
        branch="$BRANCH"
    fi

    git_clone_opts=""
    git_clone_opts="$git_clone_opts --depth 1"

    git clone $git_clone_opts -b $branch $url
}

git_clone git://git.yoctoproject.org/poky
ln -s -r -f . poky/meta-lazlo
cd poky
git_clone git://git.openembedded.org/meta-openembedded
if [ "$BRANCH" = "dunfell" ]; then
    git_clone git://git.yoctoproject.org/meta-selinux "master"
else
    git_clone git://git.yoctoproject.org/meta-selinux
fi
git_clone git://git.yoctoproject.org/meta-virtualization
