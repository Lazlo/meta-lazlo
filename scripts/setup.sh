#!/bin/sh

# Taken from https://www.yoctoproject.org/docs/3.1/ref-manual/ref-manual.html#ubuntu-packages
pkgs_yocto="gawk wget git-core diffstat unzip texinfo gcc-multilib \
	build-essential chrpath socat cpio python3 python3-pip python3-pexpect \
	xz-utils debianutils iputils-ping python3-git python3-jinja2 libegl1-mesa libsdl1.2-dev \
	pylint3 xterm"

pkgs_publish="pbzip2"
pkgs_flashing="bmap-tools dosfstools"

pkgs_misc="python-setuptools python-crypto ccache cpio locales"

pkgs="$pkgs_yocto $pkgs_publish $pkgs_flashing $pkgs_misc"

apt-get update && apt-get -q install -y $pkgs

# Make sure locales are installed and generated.
# Otherwise you will run into this message:
# "    Your system needs to support the en_US.UTF-8 locale."
echo "en_US.UTF-8 UTF-8" > /etc/locale.gen && locale-gen
