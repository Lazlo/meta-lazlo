Quick Start
===========

```
sudo ./scripts/setup.sh
./scripts/fetch.sh
. lzo-init-build-env
bitbake lazlo-image-devel
```

```
cd tmp/deploy/images/beaglebone-yocto
sudo bmaptool copy lazlo-image-devel-beaglebone-yocto.wic /dev/sdj
sudo fsck.vfat /dev/sdj1
```


Images
------

 * `lazlo-image-devel`

Images provided by Yocto Poky:

 * `core-image-minimal`
 * `core-image-full-cmdline`
 * `core-image-sato`
 * `core-image-sato-dev`
