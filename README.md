Lazlo's Yocto Project Layers
============================

Contains [meta-lzolinux](meta-lzolinux/) distro layer and [meta-lazlo](meta-lazlo/) with various recipes.

 * [Quick Start Guide](QUICKSTART.md)
 * [To Do](TODO.md)
