To Do
=====

Issues
------

 * eth0 on BeagleBone Black does not work (maybe because U-boot can not load its env because boot partition is dirty?)


Features
--------

 * have image with Docker
 * have image for on-target kernel development
 * have image with a browser that starts Grafana in fullscreen
 * have image for BeagleBone Green Wireless that is configured to be an WiFi access point
 * have image configured to be used as NFS rootfs for network boot


### Machine Features

BBB machine features we want to enable

 * bluetooth
 * keyboard
 * screen
 * serial
 * wifi

Machine features already enabled by `beaglebone-yocto.conf`

 * usbgadget
 * usbhost
 * vfat
 * alsa


### Distro Features

Ones we are interested in

 * bluetooth
 * keyboard
 * ppp
 * systemd
 * x11

Already enabled (through use of 'poky' distro)

 * alsa
 * ipv6
 * usbgadget
 * usbhost
 * wifi
 * nfs
 * zeroconf
 * opengl
 * ptest
 * wayland


### Packages

Packages we want to incorporate

 * VNC server
 * web browser
 * gpg
 * mutt
 * from openembedded-core
  * python
  * nfs-utils
  * lighttpd
  * avahi
 * from meta-openembedded/meta-python
  * python-pyserial

SORT ME
-------

 * have RT kernel
 * have reproducible builds
 * have docker running
 * have Altera FPGA support
 * have NFS client/server
 * set `ls` alias to `ls --color`

Have custom partitioning (that would fit into 4GB eMMC of BBB)

 * have /etc on extra partition (64M)
 * have /home on extra partition (1G+)
 * have /var/lib on extra partition (500M+)


### Docker Tweaks

 * Make sure locales have been installed and generated (see setup.sh for the command)
 * Make sure, the workspace is not part of the container image (uses aufs) but have
   it mounted using a volume (otherwise the wic recipe for building an image will fail).
   See [post](https://www.yoctoproject.org/pipermail/yocto/2018-July/041995.html) for details.


Interesting Layers
------------------

 * https://layers.openembedded.org/layerindex/branch/master/layer/meta-ti/
 * https://layers.openembedded.org/layerindex/branch/master/layer/meta-efi-secure-boot/
 * https://layers.openembedded.org/layerindex/branch/master/layer/meta-encrypted-storage/
 * https://layers.openembedded.org/layerindex/branch/master/layer/meta-integrity/
 * https://layers.openembedded.org/layerindex/branch/master/layer/meta-docker/
 * https://layers.openembedded.org/layerindex/branch/master/layer/meta-ci/
 * https://layers.openembedded.org/layerindex/branch/master/layer/meta-qa-framework/
 * https://layers.openembedded.org/layerindex/branch/master/layer/meta-kernel/
 * https://layers.openembedded.org/layerindex/branch/master/layer/meta-kernel-dev/
 * https://layers.openembedded.org/layerindex/branch/master/layer/meta-realtime/
 * https://layers.openembedded.org/layerindex/branch/master/layer/meta-linaro/
 * https://layers.openembedded.org/layerindex/branch/master/layer/meta-microcontroller/
 * https://layers.openembedded.org/layerindex/branch/master/layer/meta-readonly-rootfs-overlay/
 * https://layers.openembedded.org/layerindex/branch/master/layer/meta-ro-rootfs/
 * https://layers.openembedded.org/layerindex/branch/master/layer/meta-beagleboard/
 * https://layers.openembedded.org/layerindex/branch/master/layer/meta-beagleboard-extras/
 * https://layers.openembedded.org/layerindex/branch/master/layer/meta-sca/
 * https://layers.openembedded.org/layerindex/branch/master/layer/meta-security-isafw/
 * https://layers.openembedded.org/layerindex/branch/master/layer/meta-hdl/
